# NLP101



## Project Information

This is Quan's interesting project. The program test_proj is a Jupyter Notebook file with two print functions and there are three variables, with a, b, and c. The program spacy_test is a Jupyter Notebook file that produces the similar words between two songs by the band Coldplay, including all multiple instances of similar words.

A new program and another song was added to find occurrences of a noun and verb between three songs, the program will also produce a list based on a csv file, using pandas.

## Instructions
- Clone repo in DCC Jupyter Notebook
- Open more_spacy.ipynb file
- Run more_spacy.ipynb file
- Output should be how many occurences of a word between each two songs for nouns and verbs, in addition to a list of genres from the csv file


## Expected Output
Nouns matches in Yellow and Viva:  0
Nouns matches in Yellow and Sky Full of Stars:  2
Nouns matches in Viva and Sky Full of Stars: 0


Verb matches in Yellow and Viva:  0
Verb matches in Yellow and Sky Full of Stars:  0
Verb matches in Viva and Sky Full of Stars:  0


0              hip hop
1       blue-eyed soul
2                salsa
3             pop rock
4             pop punk
5      southern gospel
6            breakbeat
7        post-hardcore
8             new wave
9            breakcore
10    alternative rock
11                 ccm
12             bachata
13             chanson
14         quiet storm
15        country rock
16           chill-out
17             qawwali
18           hard rock
19        indietronica
20     classic country
21      electric blues
22           uk garage
23           math-core
24            pop rock
25         post-grunge
Name: artist.terms, dtype: object

['hip hop', 'blue-eyed soul', 'salsa', 'pop rock', 'pop punk', 'southern gospel', 'breakbeat', 'post-hardcore', 'new wave', 'breakcore', 'alternative rock', 'ccm', 'bachata', 'chanson', 'quiet storm', 'country rock', 'chill-out', 'qawwali', 'hard rock', 'indietronica', 'classic country', 'electric blues', 'uk garage', 'math-core', 'pop rock', 'post-grunge']
